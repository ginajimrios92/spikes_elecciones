#
# Author: GJ
# Mainteiners: GJ y MS
# License: Data Cívica © 2021, GPL v2 or newer
# ===============================================
# Limpiar resultados de gobernador y alcaldes de Sinaloa

if(!require(pacman)) install.packages("pacman")
pacman::p_load(tidyverse, here, janitor, rgdal, data.table, spikes)

files <- list(gubernatura=here("import/output/data_gobernadores.rds"),
              presidencia=here("import/output/data_alcaldes.rds"),
              final=here("clean-data/output/final-data.rds"),
              jefatura=here("import/output/data_jefegob.rds"))

#Traer secciones 
data <- readRDS(files$gubernatura)

#### Limpiar Gubernatura de Sinaloa ####
gubernatura <- data%>%
               mutate(zamora=rowSums(select(., pan, prd, pri,
                                      starts_with("c_pan"), starts_with("c_pri"))),
                      t=total_votos)%>%
                rename(N=lista_nominal_casilla,
                      v=zamora)

gubernatura <- select(gubernatura, N, t, v)
gubernatura <- filter(gubernatura, N>0)%>%
               filter(t<N)%>%
               filter(t>0)

aver <- spikes(gubernatura, resamples=100)


#### Limpiar Jefatura de gobierno CDMX ####
data <- readRDS(files$jefatura)
jefatura <- data%>%
            mutate(sheinbaum=rowSums(select(., pt, morena, pes,
                                     starts_with("c_comun_pt_"), 
                                     starts_with("c_comun_morena_"))),
               t=total_votos_calculados,
               N=lista_nominal_casilla)%>%
        rename(v=sheinbaum)

jefatura <- group_by(jefatura, seccion)%>%
            summarize(N=sum(N, na.rm=T),
                      t=sum(t, na.rm=T),
                      v=sum(v, na.rm=T))
jefatura <- ungroup(jefatura)
jefatura <- filter(jefatura, N>0)%>%
            filter(t<N)%>%
            filter(t>0)%>%
            select(t, N, v)

aver <- spikes(jefatura, resamples=100)

